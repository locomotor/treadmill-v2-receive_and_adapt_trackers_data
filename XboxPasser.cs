﻿using ScpDriverInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class XboxPasser : Updatable
    {
        private static XboxPasser instance;

        private ScpBus scpBus;
        private X360Controller x360Controller;

        public static bool rotate = false;

        public static Vector2 LeftStickValue
        {
            get
            {
                Vector2 returnValue = Vector2.zero;

                double up = instance.x360Controller.LeftStickX;
                double down = short.MaxValue;
                returnValue.x = up / down;

                up = instance.x360Controller.LeftStickY;
                returnValue.y = up / down;

                return returnValue;
            }

            private set
            {
                instance.x360Controller.LeftStickX = MyExtensions.Clamp((short)(value[0] * short.MaxValue), short.MinValue, short.MaxValue);
                instance.x360Controller.LeftStickY = MyExtensions.Clamp((short)(value[1] * short.MaxValue), short.MinValue, short.MaxValue);
                byte[] outputReport = null;
                instance.scpBus.Report(1, instance.x360Controller.GetReport(), outputReport);
            }
        }

        public XboxPasser() : base()
        {
            instance = this;

            x360Controller = new X360Controller();

            try
            {
                scpBus = new ScpBus();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message, "Error");
                // MessageBox.Show("Xbox constructor error " + ex.Message);
                Environment.Exit(1);
            }
        }

        protected override void Update()
        {
            try
            {
                Vector2 currentPosition = new Vector2(0, MyExtensions.Clamp(FreedomLocomotion.MoveValue, 0, 1));
                if (rotate) currentPosition.RotateAroundPoint(Vector2.zero, FreedomLocomotion.VerticalRotation);
                LeftStickValue = currentPosition;
            }
            catch (Exception ex)
            {
                // MessageBox.Show("ERROR XboxPasser THREAD " + ex.Message);
                Debug.WriteLine("ERROR XboxPasser THREAD " + ex.Message);
            }

            //x360Controller.Buttons ^= X360Buttons.A;
        }

        public static void PlugIn()
        {
            if (instance.scpBus != null)
                instance.scpBus.PlugIn(1);
        }

        public static void Unplug()
        {
            if (instance != null && instance.scpBus != null)
                instance.scpBus.UnplugAll();
        }
    }
}
