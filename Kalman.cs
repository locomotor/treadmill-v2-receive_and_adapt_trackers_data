﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class Kalman
    {
        public static double Q_angle = 0.001;// private double Qangle { get { return Q_angle; } }
        public static double Q_bias = 0.003; //private double Qbias { get { return Q_bias; } }
        public static double R_measure = 0.03; //private double Rmeasure { get { return R_measure; } }
        private double angle; public double Angle { get { return angle; } set { angle = value; } }
        private double bias; public double Bias { get { return bias; } set { bias = value; } }
        private double rate; public double Rate { get { return rate; } set { rate = value; } }

        private double[,] P;

        public Kalman()
        {
            /* We will set the variables like so, these can also be tuned by the user */
            //Q_angle = 0.001f;
            //Q_bias = 0.003f;
            //R_measure = 0.03f;

            angle = 0.0f;
            bias = 0.0f;

            P = new double[2, 2];
            P[0, 0] = 0.0f; // Since we assume that the bias is 0 and we know the starting angle (use setAngle), the error covariance matrix is set like so - see: http://en.wikipedia.org/wiki/Kalman_filter#Example_application.2C_technical
            P[0, 1] = 0.0f;
            P[1, 0] = 0.0f;
            P[1, 1] = 0.0f;
        }

        public double GetAngle(double newAngle, double newRate, double dt)
        {
            rate = newRate - bias;
            angle += dt * rate;

            P[0, 0] += dt * (dt * P[1, 1] - P[0, 1] - P[1, 0] + Q_angle);
            P[0, 1] -= dt * P[1, 1];
            P[1, 0] -= dt * P[1, 1];
            P[1, 1] += Q_bias * dt;

            // Discrete Kalman filter measurement update equations - Measurement Update ("Correct")
            // Calculate Kalman gain - Compute the Kalman gain
            /* Step 4 */
            double S = P[0, 0] + R_measure; // Estimate error
                                            /* Step 5 */
            double[] K = new double[2]; // Kalman gain - This is a 2x1 vector
            K[0] = P[0, 0] / S;
            K[1] = P[1, 0] / S;

            // Calculate angle and bias - Update estimate with measurement zk (newAngle)
            /* Step 3 */
            double y = newAngle - angle; // Angle difference
                                         /* Step 6 */
            angle += K[0] * y;
            bias += K[1] * y;

            // Calculate estimation error covariance - Update the error covariance
            /* Step 7 */
            double P00_temp = P[0, 0];
            double P01_temp = P[0, 1];

            P[0, 0] -= K[0] * P00_temp;
            P[0, 1] -= K[0] * P01_temp;
            P[1, 0] -= K[1] * P00_temp;
            P[1, 1] -= K[1] * P01_temp;

            return angle;
        }
    };
}
