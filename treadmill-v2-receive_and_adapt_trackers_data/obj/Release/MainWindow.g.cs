﻿#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "83758BDCA36E4A07D77AD35D13D4A6C90BEC8BD9F3C50DB016985A132EA33BA6"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using treadmill_v2_receive_and_adapt_trackers_data;


namespace treadmill_v2_receive_and_adapt_trackers_data {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas parentlessCanvas;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox valueMultipierTextBox;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox jawaCheckBox;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Ellipse emulatedAnalogStick;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label emulatedAnalogValue;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas javaCanvas;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_lu;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_lb;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_ru;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_rb;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button threadButton;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_t;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas repCanvas;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button tposeCalibButton;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label filterLabel;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox heightTextBox;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox sexComboBox;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox filterComboBox;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas kalmanSettingsCanvas;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider q_angleValueSlider;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label q_angleValueLabel;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider q_biasValueSlider;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label q_biasValueLabel;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider r_measureValueSlider;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label r_measureValueLabel;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label debugMessageLabel;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas quaternionFilterSettingsCanvas;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider kiValueSlider;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label kiValueLabel;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider kpValueSlider;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label kpValueLabel;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider gyroMeasErrorValueSlider;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label gyroMeasErrorValueLabel;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas simulationCanvas;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar leftLegProgressBar;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar rightLegProgressBar;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/treadmill-v2-receive_and_adapt_trackers_data;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.parentlessCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 2:
            this.valueMultipierTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 15 "..\..\MainWindow.xaml"
            this.valueMultipierTextBox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.ValueMultipierTextBox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.jawaCheckBox = ((System.Windows.Controls.CheckBox)(target));
            
            #line 16 "..\..\MainWindow.xaml"
            this.jawaCheckBox.Checked += new System.Windows.RoutedEventHandler(this.JawaCheckBox_CheckedChanged);
            
            #line default
            #line hidden
            
            #line 16 "..\..\MainWindow.xaml"
            this.jawaCheckBox.Unchecked += new System.Windows.RoutedEventHandler(this.JawaCheckBox_CheckedChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.emulatedAnalogStick = ((System.Windows.Shapes.Ellipse)(target));
            return;
            case 5:
            this.emulatedAnalogValue = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.javaCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 7:
            this.label_lu = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.label_lb = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.label_ru = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.label_rb = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.threadButton = ((System.Windows.Controls.Button)(target));
            return;
            case 12:
            this.label_t = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.repCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 14:
            this.tposeCalibButton = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\MainWindow.xaml"
            this.tposeCalibButton.Click += new System.Windows.RoutedEventHandler(this.TposeCalibButton_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.filterLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.heightTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 39 "..\..\MainWindow.xaml"
            this.heightTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.HeightTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 17:
            this.sexComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 41 "..\..\MainWindow.xaml"
            this.sexComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.SexComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 18:
            this.filterComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 46 "..\..\MainWindow.xaml"
            this.filterComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.kalmanSettingsCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 20:
            this.q_angleValueSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 52 "..\..\MainWindow.xaml"
            this.q_angleValueSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.Q_angleValueSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 21:
            this.q_angleValueLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 22:
            this.q_biasValueSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 55 "..\..\MainWindow.xaml"
            this.q_biasValueSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.Q_biasValueSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 23:
            this.q_biasValueLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 24:
            this.r_measureValueSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 58 "..\..\MainWindow.xaml"
            this.r_measureValueSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.R_measureValueSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 25:
            this.r_measureValueLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 26:
            this.debugMessageLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 27:
            this.quaternionFilterSettingsCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 28:
            this.kiValueSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 64 "..\..\MainWindow.xaml"
            this.kiValueSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.KiValueSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 29:
            this.kiValueLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.kpValueSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 67 "..\..\MainWindow.xaml"
            this.kpValueSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.KpValueSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 31:
            this.kpValueLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 32:
            this.gyroMeasErrorValueSlider = ((System.Windows.Controls.Slider)(target));
            
            #line 70 "..\..\MainWindow.xaml"
            this.gyroMeasErrorValueSlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.GyroMeasErrorValueSlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 33:
            this.gyroMeasErrorValueLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 34:
            this.simulationCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 35:
            this.leftLegProgressBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 36:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 37:
            this.rightLegProgressBar = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 38:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

