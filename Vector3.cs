﻿using System;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public struct Vector3
    {
        public static Vector3 zero = new Vector3(0, 0, 0);
        public static Vector3 one = new Vector3(1, 1, 1);
        public static Vector3 forward = new Vector3(1, 0, 0);
        public static Vector3 right = new Vector3(0, 1, 0);
        public static Vector3 up = new Vector3(0, 0, 1);

        public double x;
        public double y;
        public double z;
        public double this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                }
                return -1;
            }

            set
            {
                switch (index)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                }
            }
        }

        public Vector3(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Quaternion ToQuaternion(Vector3 v)
        {
            return ToQuaternion(v.y, v.x, v.z);
        }

        public static Quaternion ToQuaternion(double yaw, double pitch, double roll)
        {
            yaw *= 0.0174532925;
            pitch *= 0.0174532925;
            roll *= 0.0174532925;
            double rollOver2 = roll * 0.5f;
            double sinRollOver2 = Math.Sin(rollOver2);
            double cosRollOver2 = Math.Cos(rollOver2);
            double pitchOver2 = pitch * 0.5f;
            double sinPitchOver2 = Math.Sin(pitchOver2);
            double cosPitchOver2 = Math.Cos(pitchOver2);
            double yawOver2 = yaw * 0.5f;
            double sinYawOver2 = Math.Sin(yawOver2);
            double cosYawOver2 = Math.Cos(yawOver2);

            return new Quaternion
            {
                w = cosYawOver2 * cosPitchOver2 * cosRollOver2 + sinYawOver2 * sinPitchOver2 * sinRollOver2,
                x = cosYawOver2 * sinPitchOver2 * cosRollOver2 + sinYawOver2 * cosPitchOver2 * sinRollOver2,
                y = sinYawOver2 * cosPitchOver2 * cosRollOver2 - cosYawOver2 * sinPitchOver2 * sinRollOver2,
                z = cosYawOver2 * cosPitchOver2 * sinRollOver2 - sinYawOver2 * sinPitchOver2 * cosRollOver2
            };
        }
        public static Vector3 FromQuaternion(Quaternion q1)
        {
            double sqw = q1.w * q1.w;
            double sqx = q1.x * q1.x;
            double sqy = q1.y * q1.y;
            double sqz = q1.z * q1.z;
            double unit = sqx + sqy + sqz + sqw;
            double test = q1.x * q1.w - q1.y * q1.z;
            Vector3 returnValue;

            if (test > 0.4995f * unit)
            {
                returnValue.y = 2f * Math.Atan2(q1.y, q1.x);
                returnValue.x = Math.PI / 2;
                returnValue.z = 0;
                return NormalizeAngles(returnValue * 57.2958);
            }
            if (test < -0.4995f * unit)
            {
                returnValue.y = -2f * Math.Atan2(q1.y, q1.x);
                returnValue.x = -Math.PI / 2;
                returnValue.z = 0;
                return NormalizeAngles(returnValue * 57.2958);
            }

            Quaternion q = new Quaternion(q1.w, q1.z, q1.x, q1.y);
            returnValue.y = Math.Atan2(2f * q.x * q.w + 2f * q.y * q.z, 1 - 2f * (q.z * q.z + q.w * q.w));
            returnValue.x = Math.Asin(2f * (q.x * q.z - q.w * q.y));
            returnValue.z = Math.Atan2(2f * q.x * q.y + 2f * q.z * q.w, 1 - 2f * (q.y * q.y + q.z * q.z));
            return NormalizeAngles(returnValue * 57.2958);
        }

        private static Vector3 NormalizeAngles(Vector3 angles)
        {
            angles.x = NormalizeAngle(angles.x);
            angles.y = NormalizeAngle(angles.y);
            angles.z = NormalizeAngle(angles.z);
            return angles;
        }

        private static double NormalizeAngle(double angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;
            return angle;
        }

        public override string ToString()
        {
            return string.Format("Vector3({0}, {1}, {2})", x, y, z);
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2) => new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        public static Vector3 operator -(Vector3 v1, Vector3 v2) => new Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        public static Vector3 operator *(Vector3 v1, Vector3 v2) => new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
        public static Vector3 operator /(Vector3 v1, Vector3 v2) => new Vector3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
        public static Vector3 operator *(Vector3 v1, double f1) => new Vector3(v1.x * f1, v1.y * f1, v1.z * f1);
        public static Vector3 operator /(Vector3 v1, double f1) => new Vector3(v1.x / f1, v1.y / f1, v1.z / f1);
    }
}
