﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class FreedomLocomotion : Updatable
    {
        private static FreedomLocomotion instance;

        public enum DIRECTION { FORWARD = 1, IDLE = 0, BACKWARD = -1 }

        private const int numberOfEntries = 60;

        //TODO
        public static double[,] poses;
        private DIRECTION[] prevTendencies = new DIRECTION[] { DIRECTION.IDLE, DIRECTION.IDLE };


        public static double[] moveValues;
        //TODO
        public static bool[] doMove = new bool[] { true, true };
        //private int lastMovedLegIndex = 0;

        public static double moveValueMultipier = 50;
        public static double threshold = 0.005d;

        private double[] CurrentLegsPoses
        {
            get
            {
                double[] returnValue = new double[2];
                for (int i = 0; i < 2; i++)
                    returnValue[i] = Foot.Instances[i] != null ? Foot.Instances[i].Position.x : 0;
                return returnValue;
            }
        }

        public static double MoveValue
        {
            get
            {
                double returnValue = 0;
                foreach (double f in moveValues) returnValue += f < threshold ? 0 : f;
                return MyExtensions.Clamp(returnValue, 0, 1) * moveValueMultipier;
            }
        }

        public static double VerticalRotation
        {
            get
            {
                int counted = 0;
                double sum = 0;

                for (int i = 0; i < IMUData.Instances.Length; i++)
                {
                    if (IMUData.Instances[i])
                    {
                        sum += IMUData.Instances[i].EulerAngles.x;
                        counted++;
                    }
                }

                return sum / (double)counted;
            }
        }

        public static DIRECTION[] tendencies = new DIRECTION[] { DIRECTION.IDLE, DIRECTION.IDLE };
        public static int LegsCount { get { return instance.CurrentLegsPoses.Length; } }
        public static double[,] AllLegsPoses { get { return poses; } }

        public FreedomLocomotion() : base()
        {
            instance = this;
            updateIntervalMs = 5;
            poses = new double[CurrentLegsPoses.Length, numberOfEntries];
            moveValues = new double[CurrentLegsPoses.Length];
        }

        protected override void Update()
        {
            try
            {
                AddNewPrevPoses(CurrentLegsPoses);
                CaclulateTendencies();

                Move();

                for (int i = 0; i < CurrentLegsPoses.Length; i++)
                    prevTendencies[i] = tendencies[i];

                MainWindow.DispatcherObject.BeginInvoke(new Action(() =>
                {
                    MainWindow.ShowDebugMessage(moveValues[0] + "\n" + moveValues[1]);
                }));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ERROR FreedomLocomotion THREAD " + ex.Message);
                //MessageBox.Show("ERROR FreedomLocomotion THREAD " + ex.Message);
            }
        }

        private void AddNewPrevPoses(double[] newPoses)
        {
            for (int legIndex = 0; legIndex < CurrentLegsPoses.Length; legIndex++)
                for (int i = poses.GetLength(1) - 1; i >= 0; i--)
                    poses[legIndex, i] = i > 0 ? poses[legIndex, i - 1] : newPoses[legIndex];
        }

        public static int[] maxIndexs = new int[2];
        //public static Dictionary<DIRECTION, int> counter = new Dictionary<DIRECTION, int>[2];
        private void CaclulateTendencies()
        {
            for (int legIndex = 0; legIndex < CurrentLegsPoses.Length; legIndex++)
            {
                DIRECTION[] vals = new DIRECTION[poses.Length];
                Dictionary<DIRECTION, int> counter = new Dictionary<DIRECTION, int>();
                for (int i = -1; i <= 1; i++)
                    counter.Add((DIRECTION)i, 0);

                for (int i = 1; i < poses.GetLength(1); i++)
                {
                    double value = poses[legIndex, i - 1] - poses[legIndex, i];
                    if (value > 0)
                    {
                        //Debug.WriteLine("VALUE > 0");
                        counter[DIRECTION.FORWARD]++;
                        vals[i] = DIRECTION.FORWARD;
                    }
                    else if (value < 0)
                    {
                        //Debug.WriteLine("VALUE < 0");
                        counter[DIRECTION.BACKWARD]++;
                        vals[i] = DIRECTION.BACKWARD;
                    }
                    else if (value == 0)
                    {
                        //Debug.WriteLine("VALUE < 0");
                        counter[vals[i - 1]]++;
                    }
                }

                int maxIndex = 0;
                int maxValue = 0;
                for (int i = -1; i <= 1; i++)
                {
                    if (counter[(DIRECTION)i] > maxValue)
                    {
                        maxValue = counter[(DIRECTION)i];
                        maxIndex = i;
                    }
                }

                maxIndexs[legIndex] = maxIndex;

                tendencies[legIndex] = ((DIRECTION)maxIndex) == DIRECTION.IDLE ? prevTendencies[legIndex] : (DIRECTION)maxIndex;

                if (tendencies[legIndex] == DIRECTION.BACKWARD && prevTendencies[legIndex] == DIRECTION.FORWARD)
                {
                    if (doMove[1 - legIndex])
                    {
                        //lastMovedLegIndex = 1 - legIndex;
                        //doMove[1 - legIndex] = false;
                    }

                    doMove[legIndex] = true;// lastMovedLegIndex != legIndex;
                }
            }
        }

        private void Move()
        {
            for (int legIndex = 0; legIndex < CurrentLegsPoses.Length; legIndex++)
            {
                if (doMove[legIndex] /*&& lastMovedLegIndex != legIndex*/)
                {
                    double sum = 0;
                    for (int i = 1; i < AllLegsPoses.GetLength(1); i++)
                        sum += (AllLegsPoses[legIndex, i] - AllLegsPoses[legIndex, i - 1]);

                    sum /= AllLegsPoses.GetLength(1);

                    moveValues[legIndex] = MyExtensions.Clamp(sum, 0, 1);// MyExtensions.Clamp(AllLegsPoses[legIndex, 1] - AllLegsPoses[legIndex, 0], 0, MyExtensions.Infinity);

                    //Debug.WriteLine(string.Format("SUM: {0}, MOVEVALUE: {1}", sum, moveValues[legIndex]));

                    if (tendencies[legIndex] != DIRECTION.BACKWARD && tendencies[legIndex] != DIRECTION.IDLE)
                    {
                        Debug.WriteLine("CHANGE TO FALSE");
                        //doMove[legIndex] = false;
                        //lastMovedLegIndex = legIndex;
                    }
                }
                else
                {
                    moveValues[legIndex] = 0;
                }
            }
        }
    }
}
