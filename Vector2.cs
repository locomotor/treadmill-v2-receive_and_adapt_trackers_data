﻿using System;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public struct Vector2
    {
        public static Vector2 zero = new Vector2(0, 0);
        public static Vector2 one = new Vector2(1, 1);
        public static Vector2 right = new Vector2(1, 0);
        public static Vector2 up = new Vector2(0, 1);

        public double x;
        public double y;
        public double this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0: return x;
                    case 1: return y;
                }
                return -1;
            }

            set
            {
                switch (index)
                {
                    case 0: x = value; break;
                    case 1: y = value; break;
                }
            }
        }

        public Vector2(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return string.Format("Vector2({0}, {1})", x, y);
        }

        public void RotateAroundPoint(Vector2 pivot, double angle)
        {
            Vector2 newPoint = zero;

            angle *= Math.PI / 180;
            newPoint.x = Math.Cos(angle) * (x - pivot.x) - Math.Sin(angle) * (y - pivot.y) + pivot.x;
            newPoint.y = Math.Sin(angle) * (x - pivot.x) + Math.Cos(angle) * (y - pivot.y) + pivot.y;

            x = newPoint.x;
            y = newPoint.y;
        }

        public static Vector2 RotatePointAroundAnotherPoint(Vector2 pivot, double angle, Vector2 currentPosition)
        {
            Vector2 newPoint = currentPosition;

            angle *= Math.PI / 180;
            newPoint.x = Math.Cos(angle) * (currentPosition.x - pivot.x) - Math.Sin(angle) * (currentPosition.y - pivot.y) + pivot.x;
            newPoint.y = Math.Sin(angle) * (currentPosition.x - pivot.x) + Math.Cos(angle) * (currentPosition.y - pivot.y) + pivot.y;

            return newPoint;
        }

        public static Vector2 operator +(Vector2 v1, Vector2 v2) => new Vector2(v1.x + v2.x, v1.y + v2.y);
        public static Vector2 operator -(Vector2 v1, Vector2 v2) => new Vector2(v1.x - v2.x, v1.y - v2.y);
        public static Vector2 operator *(Vector2 v1, Vector2 v2) => new Vector2(v1.x * v2.x, v1.y * v2.y);
        public static Vector2 operator /(Vector2 v1, Vector2 v2) => new Vector2(v1.x / v2.x, v1.y / v2.y);
        public static Vector2 operator *(Vector2 v1, double d1) => new Vector2(v1.x * d1, v1.y * d1);
        public static Vector2 operator *(double d1, Vector2 v1) => new Vector2(v1.x * d1, v1.y * d1);
        public static Vector2 operator /(Vector2 v1, double d1) => new Vector2(v1.x / d1, v1.y / d1);
    }
}