﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class Foot : Updatable
    {
        private static Foot[] instances = new Foot[2];
        public static Foot[] Instances
        {
            get
            {
                Foot[] returnValue = new Foot[2];
                for (int i = 0; i < returnValue.Length; i++) returnValue[i] = instances[i];
                return returnValue;
            }
        }

        public enum FOOT_SIDE { LEFT, RIGHT }
        public FOOT_SIDE mySide;

        public double LeanValue => -Position.x;

        private Vector2 position;
        public Vector2 Position => new Vector2(-position.x, position.y);

        private int ThighIndex => ((int)mySide) * 2;
        private int CalfIndex => ((int)mySide) * 2 + 1;
        private IMUData ThighIMU => IMUData.Instances[ThighIndex];
        private IMUData CalfIMU => IMUData.Instances[CalfIndex];
        private double InputAxisValue => mySide == FOOT_SIDE.LEFT ? Input.Vertical0 : Input.Vertical1;
        public bool AreBothBonesConnected => ThighIMU != null && CalfIMU != null;

        public Foot(FOOT_SIDE footSIDE) : base()
        {
            mySide = footSIDE;
            instances[(int)mySide] = this;
        }

        protected override void Update()
        {
            CalculateFootPosition();
        }

        private void CalculateFootPosition()
        {
            Vector2 RotatePoint(Vector2 pivot, double angle, Vector2 currentPosition)
            {
                Vector2 newPoint = currentPosition;

                angle *= Math.PI / 180;
                newPoint.x = Math.Cos(angle) * (currentPosition.x - pivot.x) - Math.Sin(angle) * (currentPosition.y - pivot.y) + pivot.x;
                newPoint.y = Math.Sin(angle) * (currentPosition.x - pivot.x) + Math.Cos(angle) * (currentPosition.y - pivot.y) + pivot.y;

                return newPoint;
            }

            try
            {
                if (Engine.CalculateMovement)
                {
                    switch (Engine.currentMode)
                    {
                        case Engine.MODE.JAWA:
                            if (AreBothBonesConnected)
                            {
                                Vector2 calfOrigin = RotatePoint(Vector2.zero, ThighIMU.EulerAngles.y, Vector2.up * Engine.ThighLength);
                                position = RotatePoint(calfOrigin, CalfIMU.EulerAngles.y, calfOrigin + new Vector2(0, Engine.CalfLength)) / 38d;
                            }
                            break;
                        case Engine.MODE.SIMULATION:
                            position.x = InputAxisValue;
                            break;
                    }
                }
            }
            catch (Exception ex) {
               // MessageBox.Show("ERROR Foot THREAD " + ex.Message);
                Debug.WriteLine("ERROR Foot THREAD " + ex.Message); }
        }
    }
}
