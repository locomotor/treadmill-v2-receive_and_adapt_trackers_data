﻿using System;
using System.Diagnostics;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class IMUData
    {
        private static IMUData[] instances = new IMUData[5];
        public static IMUData[] Instances
        {
            get
            {
                //IMUData[] returnValue = new IMUData[instances.Length];
                //for (int i = 0; i < returnValue.Length; i++) returnValue[i] = instances[i];
                return instances;
            }
        }

        public static bool IsAnyIMUReceivingRawData
        {
            get
            {
                foreach (IMUData imud in instances)
                    if (imud != null && imud.dataReceivedType == DATA_RECEIVED_TYPE.RAW)
                        return true;

                return false;
            }
        }

        public enum FILTER { KALMAN, MADGWICK, MAHONY }
        public static FILTER filterUsed = FILTER.KALMAN;

        private enum DATA_RECEIVED_TYPE { NONE_SO_FAR, EULER, QUATERNION, RAW }
        private DATA_RECEIVED_TYPE dataReceivedType = DATA_RECEIVED_TYPE.NONE_SO_FAR;

        public Vector3 accelerometer;
        public Vector3 gyroscop;
        public Vector3 magnetometer;

        private Kalman kalmanY = new Kalman();
        private Kalman kalmanX = new Kalman();
        private Madgwick madgwick = new Madgwick();
        private Mahony mahony = new Mahony();
        private Vector3 calibrationDifference;
        private double gyroXangle = 0;
        private double gyroYangle = 0;
        private double compAngleX = 0;
        private double compAngleY = 0;
        private double deltaTime = 0;
        private double prevdeltaTime = 0;
        private Quaternion quaternionRotation;
        private Vector3 eulerRotation;

        public Quaternion Quaternion => quaternionRotation;
        public Vector3 EulerAngles => eulerRotation -calibrationDifference;

        public IMUData(double[] data)
        {
            if (data.Length == 3)
                Update(data[0], data[1], data[2]);
            else if (data.Length == 4)
                Update(data[0], data[1], data[2], data[3]);
            else if (data.Length == 9)
                InitializeIMUData(new Vector3(data[0], data[1], data[2]), new Vector3(data[3], data[4], data[5]), new Vector3(data[6], data[7], data[8]));
        }

        private void InitializeIMUData(double ax, double ay, double az, double gx, double gy, double gz, double mx, double my, double mz) => InitializeIMUData(new Vector3(ax, ay, az), new Vector3(gx, gy, gz), new Vector3(mx, my, mz));

        private void InitializeIMUData(Vector3 accelerometer, Vector3 gyroscop, Vector3 magnetometer)
        {
            this.accelerometer = accelerometer;
            this.gyroscop = gyroscop;
            this.magnetometer = magnetometer;

            double roll = Math.Atan2(accelerometer.y, accelerometer.z) * Math.PI / 180d;
            double pitch = Math.Atan(-accelerometer.x / Math.Sqrt(Math.Pow(accelerometer.y, 2) + Math.Pow(accelerometer.z, 2))) * Math.PI / 180d;

            kalmanX.Angle = gyroXangle = compAngleX = roll;
            kalmanY.Angle = gyroYangle = compAngleY = pitch;
            dataReceivedType = DATA_RECEIVED_TYPE.RAW;
        }

        public void Update(double[] data)
        {
            if (data.Length == 3)
                Update(data[0], data[1], data[2]);
            else if (data.Length == 4)
                Update(data[0], data[1], data[2], data[3]);
            else if (data.Length == 9)
                Update(new Vector3(data[0], data[1], data[2]), new Vector3(data[3], data[4], data[5]), new Vector3(data[6], data[7], data[8]));
        }

        public void Update(double ax, double ay, double az, double gx, double gy, double gz, double mx, double my, double mz) => Update(new Vector3(ax, ay, az), new Vector3(gx, gy, gz), new Vector3(mx, my, mz));

        public void Update(double x, double y, double z)
        {
            eulerRotation = new Vector3(x, y, z);
            dataReceivedType = DATA_RECEIVED_TYPE.EULER;
        }

        public void Update(double x, double y, double z, double w)
        {
            quaternionRotation = new Quaternion(x, y, z, w);
            eulerRotation = Vector3.FromQuaternion(quaternionRotation);
            dataReceivedType = DATA_RECEIVED_TYPE.QUATERNION;
        }

        public void Update(Vector3 accelerometer, Vector3 gyroscop, Vector3 magnetometer)
        {
            deltaTime = (DateTime.Now.Ticks - prevdeltaTime) / 1000000f;
            prevdeltaTime = DateTime.Now.Ticks;

            switch (filterUsed)
            {
                case FILTER.KALMAN:
                    KalmanUpdate(accelerometer, gyroscop, magnetometer);
                    break;
                case FILTER.MADGWICK:
                    MadgwickUpdate(accelerometer, gyroscop, magnetometer);
                    break;
                case FILTER.MAHONY:
                    MahonyUpdate(accelerometer, gyroscop, magnetometer);
                    break;
            }
            dataReceivedType = DATA_RECEIVED_TYPE.RAW;
        }

        private void KalmanUpdate(Vector3 accelerometer, Vector3 gyroscop, Vector3 magnetometer)
        {
            double roll = Math.Atan2(accelerometer.y, accelerometer.z) * 57.29;
            double pitch = Math.Atan(-accelerometer.x / Math.Sqrt(Math.Pow(accelerometer.y, 2) + Math.Pow(accelerometer.z, 2))) * Math.PI / 180d;

            double gyroXrate = gyroscop.x / 131.0;
            double gyroYrate = gyroscop.y / 131.0;

            if ((roll < -90 && eulerRotation.y > 90) || (roll > 90 && eulerRotation.y < -90))
            {
                kalmanX.Angle = compAngleX = eulerRotation.y = gyroXangle = roll;
            }
            else
            {
                eulerRotation.y = kalmanX.GetAngle(roll, gyroXrate, deltaTime);
            }

            if (Math.Abs(eulerRotation.y) > 90)
            {
                gyroYrate = -gyroYrate;
            }

            eulerRotation.x = kalmanY.GetAngle(pitch, gyroYrate, deltaTime);

            gyroXangle += gyroXrate * deltaTime;
            gyroYangle += gyroYrate * deltaTime;

            compAngleX = 0.93 * (compAngleX + gyroXrate * deltaTime) + 0.07 * roll;
            compAngleY = 0.93 * (compAngleY + gyroYrate * deltaTime) + 0.07 * pitch;

            if (gyroXangle < -180 || gyroXangle > 180)
            {
                gyroXangle = eulerRotation.y;
            }
            if (gyroYangle < -180 || gyroYangle > 180)
            {
                gyroYangle = eulerRotation.x;
            }
        }
        
        private void MadgwickUpdate(Vector3 accelerometer, Vector3 gyroscop, Vector3 magnetometer)
        {
            Quaternion returnedQuaternion = Quaternion.identity;
            madgwick.Update(accelerometer, gyroscop, magnetometer, deltaTime, ref returnedQuaternion);

            gyroXangle = Vector3.FromQuaternion(returnedQuaternion).x;
        }

        private void MahonyUpdate(Vector3 accelerometer, Vector3 gyroscop, Vector3 magnetometer)
        {
            Quaternion returnedQuaternion = Quaternion.identity;
            mahony.Update(accelerometer, gyroscop, magnetometer, deltaTime, ref returnedQuaternion);

            gyroXangle = Vector3.FromQuaternion(returnedQuaternion).x;
        }

        public void Calibrate()
        {
            calibrationDifference = eulerRotation;
        }

        public static implicit operator bool(IMUData o1) => o1 != null;
    }
}