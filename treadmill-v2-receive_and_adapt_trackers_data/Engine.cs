﻿using ScpDriverInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class Engine
    {
        public static double ThighLength => instance.userHeight * AvThighLen / AvHeight;
        public static double CalfLength => instance.userHeight * AvCalfLen / AvHeight;
        private static double AvHeight => userSex == SEX.MALE ? 175.4 : 162.56;
        private static double AvThighLen => userSex == SEX.MALE ? 48 : 44.45;
        private static double AvCalfLen => userSex == SEX.MALE ? 38.735 : 35.5064;

        private static Engine instance = null;

        public enum MODE { JAWA, SIMULATION }
        public static MODE currentMode = MODE.JAWA;
        public enum SEX { MALE, FEMALE }
        public static SEX userSex = SEX.MALE;

        private double userHeight = 175.4;
        private static TcpListener trackersListener;
        private static Thread acceptingClientsThread;
        private static List<Thread> receiveIMUData_threads = new List<Thread>();

        public static bool RunThreads = false;
        public static bool IsAcceptingClients => acceptingClientsThread != null && acceptingClientsThread.IsAlive;
        public static bool CalculateMovement => IsAcceptingClients || currentMode == MODE.SIMULATION;

        public Engine()
        {
            instance = this;
            RunThreads = true;
            new Input();
            new Time();
            new FreedomLocomotion();
            new XboxPasser();
            for (int i = 0; i < 2; i++) new Foot((Foot.FOOT_SIDE)i);

            ThreadPool.QueueUserWorkItem(o =>
            {
                while (Engine.RunThreads)
                {
                    Thread.Sleep(100);
                    MainWindow.DispatcherObject.BeginInvoke(new Action(() =>
                    {
                        SlowUpdate();
                    }));
                }
            });
        }

        private void SlowUpdate()
        {
            //Debug.WriteLine("AreBothBonesConnected " + Foot.Instances[0].AreBothBonesConnected + " " + Foot.Instances[1].AreBothBonesConnected);
            //Debug.WriteLine("doMove " + FreedomLocomotion.doMove[0] + " " + FreedomLocomotion.doMove[1]);
            //Debug.WriteLine("tendencies " + FreedomLocomotion.tendencies[0] + " " + FreedomLocomotion.tendencies[1]);
            //Debug.WriteLine("maxIndexes " + FreedomLocomotion.maxIndexs[0] + " " + FreedomLocomotion.maxIndexs[1]);
            //Debug.WriteLine("poses " + FreedomLocomotion.counter[FreedomLocomotion.DIRECTION.BACKWARD] + " " + FreedomLocomotion.counter[FreedomLocomotion.DIRECTION.IDLE] + " " + FreedomLocomotion.counter[FreedomLocomotion.DIRECTION.FORWARD]);
            //Debug.WriteLine("CalculateMovement " + Engine.CalculateMovement);
            //Debug.WriteLine("mode " + Engine.currentMode);
            //Debug.WriteLine("poses " + FreedomLocomotion.poses[0, 0] + " " + FreedomLocomotion.poses[0, 1]+ " " + FreedomLocomotion.poses[0, 2]+ " " + FreedomLocomotion.poses[0, 3]);
            //Debug.WriteLine("LEFT " + Foot.Instances[(int)Foot.FOOT_SIDE.LEFT].position);
            //Debug.WriteLine("RIGHT " + Foot.Instances[(int)Foot.FOOT_SIDE.RIGHT].position);
            //Debug.WriteLine("MoveValue " + FreedomLocomotion.moveValues[0] + " " + FreedomLocomotion.moveValues[1]);
        }

        public static void ToggleMode()
        {
            currentMode = currentMode == MODE.JAWA ? MODE.SIMULATION : MODE.JAWA;
        }

        public static void RunAcceptingClientsThread()
        {
            XboxPasser.PlugIn();
            trackersListener = TcpListener.Create(13000);
            trackersListener.Start();

            acceptingClientsThread = new Thread(() =>
            {
                instance.MetodaTymczasowa();
            });

            acceptingClientsThread.Start();
        }

        private void MetodaTymczasowa()
        {
            try
            {
                while (true)
                {
                    TcpClient tcpClient = trackersListener.AcceptTcpClient();

                    receiveIMUData_threads.Add(new Thread(() =>
                    {
                        Debug.WriteLine("CHŁOP POŁĄCZONY");
                        MainWindow.ShowDebugMessage("CHŁOP POŁĄCZONY");
                        try
                        {
                            NetworkStream networkStream = tcpClient.GetStream();
                            byte[] buffer = new byte[1];
                            string receivedString = string.Empty;
                            string endMessageString = string.Empty;
                            string lastMessage = string.Empty;
                            bool zero = false;
                            bool one = false;
                            bool two = false;

                            while (networkStream.Read(buffer, 0, buffer.Length) != 0)
                            {
                                receivedString = Encoding.ASCII.GetString(buffer);
                                zero = true;
                                if (string.IsNullOrWhiteSpace(receivedString))
                                    continue;
                                one = true;
                                if (endMessageString == string.Empty)
                                {
                                    switch (receivedString)
                                    {
                                        case "A":
                                        case "B":
                                        case "C":
                                        case "D":
                                        case "T":
                                            endMessageString = receivedString;
                                            break;
                                    }
                                }

                                if (receivedString != endMessageString)
                                {
                                    lastMessage = (lastMessage == string.Empty) ? receivedString : string.Concat(lastMessage, receivedString);
                                    two = true;
                                }

                                if (receivedString == endMessageString)
                                {
                                    ProcessReceivedDataPack(lastMessage, endMessageString);
                                    lastMessage = string.Empty;
                                }

                                buffer = new byte[1];
                            }
                        }
                        catch (Exception ex)
                        {
                            int lineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();

                            Debug.WriteLine("OJEJKU + linenumber: " + lineNumber + " + " + ex.Message);
                            //MessageBox.Show("OJEJKU + linenumber: " + lineNumber + " + " + ex.Message);
                        }
                    }));

                    receiveIMUData_threads[receiveIMUData_threads.Count - 1].Start();
                }
            }
            catch (Exception ex)
            {
                int lineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();

                Debug.WriteLine("OJEJKU2 + linenumber: " + lineNumber + " + " + ex.Message);
                //MessageBox.Show("OJEJKU2 + linenumber: " + lineNumber + " + " + ex.Message);
            }
        }

        public static void StopConnections()
        {
            for (int i = 0; i < IMUData.Instances.Length; i++)
                IMUData.Instances[i] = null;

            XboxPasser.Unplug();
            trackersListener.Stop();
            acceptingClientsThread.Abort();
            foreach (Thread t in receiveIMUData_threads) t.Abort();
        }

        public static void CalibrateImus()
        {
            foreach (IMUData imu in IMUData.Instances) imu?.Calibrate();
        }

        public static void RunThreadDeletingRedundantThreads()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                while (IsAcceptingClients && Engine.RunThreads)
                {
                    for (int i = 0; i < receiveIMUData_threads.Count; i++)
                    {
                        if (!receiveIMUData_threads[i].IsAlive)
                        {
                            receiveIMUData_threads.RemoveAt(i--);
                            Debug.WriteLine("CHŁOP ROZŁĄCZONY");
                        }
                    }

                    Thread.Sleep(1);
                }
            });
        }

        private static void ProcessReceivedDataPack(string lastMessage, string endMessageString)
        {
            try
            {
                lastMessage = lastMessage.Replace(",", ".");

                int index = -1;

                switch (endMessageString)
                {
                    case "A": index = 0; break;
                    case "B": index = 1; break;
                    case "C": index = 2; break;
                    case "D": index = 3; break;
                    case "T": index = 4; break;
                }

                if (lastMessage.Length == 0) return;

                lastMessage = lastMessage.Substring(0, lastMessage.Length - 1);
                string[] dataSplit = lastMessage.Split('|');
                //= double.Parse(lastMessage) - 90;

                double[] parsedDataSplit = new double[dataSplit.Length];
                for (int i = 0; i < parsedDataSplit.Length; i++)
                {
                    try
                    {
                        var culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
                        culture.NumberFormat.NumberDecimalSeparator = ".";
                        parsedDataSplit[i] = double.Parse(dataSplit[i], culture);
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("OJEJKU4 " + dataSplit[i] + " " + ex.Message);
                    }

                    //if (!double.TryParse(dataSplit[i], out parsedDataSplit[i]))
                    //{
                    //}
                }

                if (!IMUData.Instances[index])
                    IMUData.Instances[index] = new IMUData(parsedDataSplit);

                IMUData.Instances[index].Update(parsedDataSplit);

                //if (index == 4)
                //   Debug.WriteLine(IMUData.Instances[index].EulerAngles.ToString());
            }
            catch (Exception ex)
            {
               // MessageBox.Show("OJEJKU3 " + new StackTrace(ex, true).GetFrame(0).GetFileLineNumber() + " " + ex.Message);
            }
        }
    }
}
