﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class Time : Updatable
    {
        private static Time instance;

        private double previousTime;
        private Stopwatch stopwatch;

        public static double deltaTime;
        public static double timeSinceStart;

        public Time() : base()
        {
            instance = this;

            stopwatch = new Stopwatch();
            stopwatch.Start();
        }

        protected override void Update()
        {
            stopwatch.Stop();
            deltaTime = (stopwatch.ElapsedMilliseconds - previousTime)/1000;
            previousTime = stopwatch.ElapsedMilliseconds;
            timeSinceStart += deltaTime;
            stopwatch.Start();
        }
    }
}
