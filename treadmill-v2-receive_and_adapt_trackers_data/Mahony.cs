﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class Mahony : QuaternionFilter
    {
        protected double[] eInt = { 0.0, 0.0, 0.0 };

        public void Update(Vector3 accelerometer, Vector3 gyroscop, Vector3 magnetometer, double deltat, ref Quaternion q)
        {
            Update(accelerometer.x, accelerometer.y, accelerometer.z,
                gyroscop.x, gyroscop.y, gyroscop.z,
                magnetometer.x, magnetometer.y, magnetometer.z,
                deltat, ref q);
        }

        public void Update(double ax, double ay, double az, double gx, double gy, double gz, double mx, double my, double mz, double deltat, ref Quaternion q)
        {
            //Debug.LogFormat("ax: {0}, ay: {1}, az: {2}, gx: {3}, gy: {4}, gz: {5}, mx: {6}, my: {7}, mz: {8}, deltat: {9}",
            //    ax, ay, az, gx, gy, gz, mx, my, mz, deltat);

            double q1 = q[0], q2 = q[1], q3 = q[2], q4 = q[3];   // short name local variable for readability
            double norm;
            double hx, hy, bx, bz;
            double vx, vy, vz, wx, wy, wz;
            double ex, ey, ez;
            double pa, pb, pc;

            // Auxiliary variables to avoid repeated arithmetic
            double q1q1 = q1 * q1;
            double q1q2 = q1 * q2;
            double q1q3 = q1 * q3;
            double q1q4 = q1 * q4;
            double q2q2 = q2 * q2;
            double q2q3 = q2 * q3;
            double q2q4 = q2 * q4;
            double q3q3 = q3 * q3;
            double q3q4 = q3 * q4;
            double q4q4 = q4 * q4;

            // Normalise accelerometer measurement
            norm = Math.Sqrt(ax * ax + ay * ay + az * az);
            if (norm == 0.0) return; // handle NaN
            norm = 1.0 / norm;        // use reciprocal for division
            ax *= norm;
            ay *= norm;
            az *= norm;

            // Normalise magnetometer measurement
            norm = Math.Sqrt(mx * mx + my * my + mz * mz);
            if (norm == 0.0) return; // handle NaN
            norm = 1.0 / norm;        // use reciprocal for division
            mx *= norm;
            my *= norm;
            mz *= norm;

            // Reference direction of Earth's magnetic field
            hx = 2.0 * mx * (0.5 - q3q3 - q4q4) + 2.0 * my * (q2q3 - q1q4) + 2.0 * mz * (q2q4 + q1q3);
            hy = 2.0 * mx * (q2q3 + q1q4) + 2.0 * my * (0.5 - q2q2 - q4q4) + 2.0 * mz * (q3q4 - q1q2);
            bx = Math.Sqrt((hx * hx) + (hy * hy));
            bz = 2.0 * mx * (q2q4 - q1q3) + 2.0 * my * (q3q4 + q1q2) + 2.0 * mz * (0.5 - q2q2 - q3q3);

            // Estimated direction of gravity and magnetic field
            vx = 2.0 * (q2q4 - q1q3);
            vy = 2.0 * (q1q2 + q3q4);
            vz = q1q1 - q2q2 - q3q3 + q4q4;
            wx = 2.0 * bx * (0.5 - q3q3 - q4q4) + 2.0 * bz * (q2q4 - q1q3);
            wy = 2.0 * bx * (q2q3 - q1q4) + 2.0 * bz * (q1q2 + q3q4);
            wz = 2.0 * bx * (q1q3 + q2q4) + 2.0 * bz * (0.5 - q2q2 - q3q3);

            // Error is cross product between estimated direction and measured direction of gravity
            ex = (ay * vz - az * vy) + (my * wz - mz * wy);
            ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
            ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
            if (ki > 0.0)
            {
                eInt[0] += ex;      // accumulate integral error
                eInt[1] += ey;
                eInt[2] += ez;
            }
            else
            {
                eInt[0] = 0.0;     // prevent integral wind up
                eInt[1] = 0.0;
                eInt[2] = 0.0;
            }

            // Apply feedback terms
            gx = gx + kp * ex + ki * eInt[0];
            gy = gy + kp * ey + ki * eInt[1];
            gz = gz + kp * ez + ki * eInt[2];

            // Integrate rate of change of quaternion
            pa = q2;
            pb = q3;
            pc = q4;
            q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5 * deltat);
            q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5 * deltat);
            q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5 * deltat);
            q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5 * deltat);

            // Normalise quaternion
            norm = Math.Sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
            norm = 1.0 / norm;
            q[0] = q1;// * norm;
            q[1] = q2;// * norm;
            q[2] = q3;// * norm;
            q[3] = q4;// * norm;

            //MainWindow.ShowDebugMessage(q.ToString());
        }
    }
}
