﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public abstract class Updatable
    {
        protected int updateIntervalMs = 1;
        protected bool runInDispatcher = true;

        public Updatable()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                while (Engine.RunThreads)
                {
                    Thread.Sleep(updateIntervalMs);

                    if (runInDispatcher)
                        MainWindow.DispatcherObject.BeginInvoke(new Action(() =>
                        {
                            Update();
                        }));
                    else
                        Update();
                }
            });
        }

        protected abstract void Update();
    }
}
