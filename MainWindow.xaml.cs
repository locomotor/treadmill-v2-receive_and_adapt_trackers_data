﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public partial class MainWindow : Window
    {
        private static MainWindow instance;

        private Line[,] IMURepresentations = new Line[3, 5];
        private Line[] IMURepresentations_Y = new Line[5];
        private Line[] IMURepresentations_Z = new Line[5];
        private Canvas[] AllCanvases { get { return new Canvas[] { kalmanSettingsCanvas, quaternionFilterSettingsCanvas, quaternionFilterSettingsCanvas }; } }

        private bool windowsInitialized = false;
        //private int valueToSetForLabels = -1;

        public static Dispatcher DispatcherObject { get { return instance.Dispatcher; } }

        public MainWindow()
        {
            instance = this;
            InitializeComponent();
            new Engine();

            UpdateLabels();

            threadButton.Click += ThreadButton_Click_Start;
            ComboBox_SelectionChanged(null, null);
            windowsInitialized = true;

            ThreadPool.QueueUserWorkItem(o =>
            {
                Thread.Sleep(100);
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    GyroMeasErrorValueSlider_ValueChanged(null, null);
                    KiValueSlider_ValueChanged(null, null);
                    KpValueSlider_ValueChanged(null, null);
                    Q_angleValueSlider_ValueChanged(null, null);
                    Q_biasValueSlider_ValueChanged(null, null);
                    R_measureValueSlider_ValueChanged(null, null);
                }));
            });
        }

        public static void ShowDebugMessage(string message)
        {
            if (instance == null || instance.debugMessageLabel == null) return;
            instance.Dispatcher.BeginInvoke(new Action(() =>
            {
                instance.debugMessageLabel.Content = message;
            }));
        }

        private void UpdateLabels()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                while (Engine.RunThreads)
                {
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if (Engine.IsAcceptingClients)
                        {
                            label_lu.Content = IMUData.Instances[0] ? IMUData.Instances[0].EulerAngles.y.ToString() : "---";
                            label_lb.Content = IMUData.Instances[1] ? IMUData.Instances[1].EulerAngles.y.ToString() : "---";
                            label_ru.Content = IMUData.Instances[2] ? IMUData.Instances[2].EulerAngles.y.ToString() : "---";
                            label_rb.Content = IMUData.Instances[3] ? IMUData.Instances[3].EulerAngles.y.ToString() : "---";
                            label_t.Content = IMUData.Instances[4] ? IMUData.Instances[4].EulerAngles.y.ToString() : "---";

                            DrawRepresentations_X();
                            DrawRepresentations_Y();

                            //Canvas.SetLeft(emulatedAnalogStick, -75 - XboxPasser.LeftStickValue.x * 30);
                            //Canvas.SetTop(emulatedAnalogStick, 100 - XboxPasser.LeftStickValue.y * 30);
                            //emulatedAnalogValue.Content = Math.Round(XboxPasser.LeftStickValue.y, 4);
                            Canvas.SetLeft(emulatedAnalogStick, -75 - Keen.XAxis * 30);
                            Canvas.SetTop(emulatedAnalogStick, 100 - Keen.YAxis * 30);
                            emulatedAnalogValue.Content = Math.Round(Keen.YAxis, 4);
                        }

                        leftLegProgressBar.Value = Input.Vertical0;
                        rightLegProgressBar.Value = Input.Vertical1;

                        //filterComboBox.Visibility = IMUData.IsAnyIMUReceivingRawData ? Visibility.Visible : Visibility.Hidden;
                        //filterLabel.Visibility = IMUData.IsAnyIMUReceivingRawData ? Visibility.Visible : Visibility.Hidden;
                        //foreach (Canvas c in AllCanvases) c.Visibility = IMUData.IsAnyIMUReceivingRawData ? Visibility.Visible : Visibility.Hidden;
                    }));

                    Thread.Sleep(1);
                }
            });
        }

        private void DrawRepresentations_Y()
        {
            Vector2 GetOrigin(int IMUIndex)
            {
                Vector2[] origins = new Vector2[] {
                    new Vector2(100, 50),
                    new Vector2(100, 100),
                    new Vector2(200, 50),
                    new Vector2(200, 100),
                    new Vector2(300, 50),
                    new Vector2(300, 100)
                };

                return IMUIndex % 2 == 0 ?
                    origins[IMUIndex] :
                    Vector2.RotatePointAroundAnotherPoint(origins[IMUIndex - 1],
                        IMUData.Instances[IMUIndex - 1] ?
                        IMUData.Instances[IMUIndex - 1].EulerAngles.y :
                        0,
                    origins[IMUIndex - 1] + new Vector2(0, 50))
                    ;
            }

            for (int i = 0; i < 5; i++)
            {
                if (IMUData.Instances[i])
                {
                    Vector2 origin = GetOrigin(i);
                    if (IMURepresentations_Y[i] == null)
                    {
                        IMURepresentations_Y[i] = new Line();
                        IMURepresentations_Y[i].Stroke = Brushes.Black;
                        IMURepresentations_Y[i].StrokeThickness = 5;
                        repCanvas.Children.Add(IMURepresentations_Y[i]);
                    }

                    IMURepresentations_Y[i].X1 = origin.x;
                    IMURepresentations_Y[i].Y1 = origin.y;

                    Vector2 rotatedPosition = Vector2.RotatePointAroundAnotherPoint(origin, IMUData.Instances[i].EulerAngles.y, origin + new Vector2(0, 50));
                    IMURepresentations_Y[i].X2 = double.IsNaN(rotatedPosition.x) ? 0 : rotatedPosition.x;
                    IMURepresentations_Y[i].Y2 = double.IsNaN(rotatedPosition.y) ? 0 : rotatedPosition.y;
                }
                else
                {
                    if (IMURepresentations_Y[i] != null)
                    {
                        repCanvas.Children.Remove(IMURepresentations_Y[i]);
                        IMURepresentations_Y[i] = null;
                    }
                }
            }
        }

        private void DrawRepresentations_X()
        {
            Vector2 GetOrigin(int IMUIndex)
            {
                Vector2[] origins = new Vector2[]
                {
                    new Vector2(150, 200),
                    new Vector2(200, 100)
                };

                return origins[IMUIndex];
            }

            for (int i = 0; i < IMUData.Instances.Length; i++)
            {
                int reprIndex = i < 4 ? 0 : 1;
                if (IMUData.Instances[i])
                {
                    Vector2 origin = GetOrigin(reprIndex);
                    if (IMURepresentations_Z[reprIndex] == null)
                    {
                        IMURepresentations_Z[reprIndex] = new Line();
                        IMURepresentations_Z[reprIndex].Stroke = Brushes.Green;
                        IMURepresentations_Z[reprIndex].StrokeThickness = 5;
                        repCanvas.Children.Add(IMURepresentations_Z[reprIndex]);
                    }

                    IMURepresentations_Z[reprIndex].X1 = origin.x;
                    IMURepresentations_Z[reprIndex].Y1 = origin.y;

                    Vector2 rotatedPosition = Vector2.RotatePointAroundAnotherPoint(origin, FreedomLocomotion.VerticalRotation, origin + new Vector2(0, 50));
                    IMURepresentations_Z[reprIndex].X2 = double.IsNaN(rotatedPosition.x) ? 0 : rotatedPosition.x;
                    IMURepresentations_Z[reprIndex].Y2 = double.IsNaN(rotatedPosition.y) ? 0 : rotatedPosition.y;
                }
                else
                {
                    if (IMURepresentations_Z[reprIndex] != null)
                    {
                        repCanvas.Children.Remove(IMURepresentations_Z[i]);
                        IMURepresentations_Z[reprIndex] = null;
                    }
                }
            }
        }

        private void JawaCheckBox_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (!windowsInitialized) return;
            Engine.ToggleMode();

            javaCanvas.Visibility = javaCanvas.Visibility == Visibility.Hidden ? Visibility.Visible : Visibility.Hidden;
            simulationCanvas.Visibility = simulationCanvas.Visibility == Visibility.Hidden ? Visibility.Visible : Visibility.Hidden;
        }

        #region JAWA_METHODS
        //<-------------------------------------------- J A W A   M E T H O D S -------------------------------------------->
        public void ThreadButton_Click_Start(object sender, RoutedEventArgs e)
        {
            Engine.RunAcceptingClientsThread();
            //valueToSetForLabels = -1;
            Debug.WriteLine("ThreadButton_Click_Start");

            threadButton.Content = "STOP SERVER";
            threadButton.Click -= ThreadButton_Click_Start;
            threadButton.Click += ThreadButton_Click_Stop;
        }

        public void ThreadButton_Click_Stop(object sender, RoutedEventArgs e)
        {
            // valueToSetForLabels=0;
            Engine.StopConnections();
            Debug.WriteLine("ThreadButton_Click_Stop");

            threadButton.Content = "START SERVER";
            threadButton.Click -= ThreadButton_Click_Stop;
            threadButton.Click += ThreadButton_Click_Start;
        }

        private void TposeCalibButton_Click(object sender, RoutedEventArgs e) => Engine.CalibrateImus();

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!windowsInitialized) return;

            IMUData.filterUsed = (IMUData.FILTER)filterComboBox.SelectedIndex;
            foreach (Canvas c in AllCanvases)
                c.Visibility = Visibility.Hidden;

            AllCanvases[filterComboBox.SelectedIndex].Visibility = Visibility.Visible;
        }

        private void Q_angleValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!windowsInitialized) return;
            Kalman.Q_angle = q_angleValueSlider.Value;
            q_angleValueLabel.Content = q_angleValueSlider.Value;
        }

        private void Q_biasValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!windowsInitialized) return;
            Kalman.Q_bias = q_biasValueSlider.Value;
            q_biasValueLabel.Content = q_biasValueSlider.Value;
        }

        private void R_measureValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!windowsInitialized) return;
            Kalman.R_measure = r_measureValueSlider.Value;
            r_measureValueLabel.Content = r_measureValueSlider.Value;
        }

        private void KiValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!windowsInitialized) return;
            QuaternionFilter.ki = kiValueSlider.Value;
            kiValueLabel.Content = kiValueSlider.Value;
        }

        private void KpValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!windowsInitialized) return;
            QuaternionFilter.kp = kpValueSlider.Value;
            kpValueLabel.Content = kpValueSlider.Value;
        }

        private void GyroMeasErrorValueSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!windowsInitialized) return;
            QuaternionFilter.gyroMeasErrornominator = gyroMeasErrorValueSlider.Value;
            gyroMeasErrorValueLabel.Content = gyroMeasErrorValueSlider.Value;
        }
        #endregion

        #region SIMULATION_METHODS
        //<-------------------------------------------- S I M U L A T I O N   M E T H O D S -------------------------------------------->
        #endregion

        private void ValueMultipierTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!windowsInitialized) return;
            string text = valueMultipierTextBox.Text;
            text = text.Replace(",", ".");

            FreedomLocomotion.moveValueMultipier = double.TryParse(text, out double result) ? result : 50;
        }

        private void ThresholdTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!windowsInitialized) return;
            string text = thresholdTextBox.Text;
            text = text.Replace(",", ".");

            FreedomLocomotion.threshold = double.TryParse(text, out double result) ? result : 50;
        }

        private void HeightTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!windowsInitialized) return;
            Regex regex = new Regex("0*[1-9][0-9]*");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SexComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!windowsInitialized) return;
            Engine.userSex = ((ComboBoxItem)sexComboBox.SelectedValue).Content.ToString() == "Male" ? Engine.SEX.MALE : Engine.SEX.FEMALE;
            //Debug.WriteLine(((ComboBoxItem)sexComboBox.SelectedValue).Content.ToString()=="Male");
        }

        private void RotateCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            XboxPasser.rotate = RotateCheckBox.IsChecked.GetValueOrDefault();
        }

        private void controllerIDTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!windowsInitialized) return;
            //Regex regex = new Regex("0*[1-9][0-9]*");
            //e.Handled = regex.IsMatch(e.Text);
        }

        private void controllerIDTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!windowsInitialized) return;
            if (int.TryParse(controllerIDTextBox.Text, out int result))
                Keen.controllerID = result;
        }

        private void LinkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!windowsInitialized) return;
            Keen.linkToCmdline = LinkTextBox.Text;
        }

        private void IntervalTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!windowsInitialized) return;
            if (int.TryParse(intervalTextBox.Text, out int result))
                Keen.Interval = result < 1 ? 1 : result;
        }

        private void intervalTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!windowsInitialized) return;
        }

        private void MethodComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!windowsInitialized) return;
            Keen.CurrentMethod = (Keen.METHOD)methodComboBox.SelectedIndex;
        }
    }
}