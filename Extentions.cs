﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public static class MyExtensions
    {
        public static double Infinity { get { return double.MaxValue; } }

        public static double Lerp(double start, double end, double step)
        {
            return start * (1 - step) + end * step;
        }

        public static double Clamp(double value, double min, double max)
        {
            return value < min ? min : (value > max ? max : value);
        }

        public static short Clamp(short value, short min, short max)
        {
            return value < min ? min : (value > max ? max : value);
        }
    }
}
