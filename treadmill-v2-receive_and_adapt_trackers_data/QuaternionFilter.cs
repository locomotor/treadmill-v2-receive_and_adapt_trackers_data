﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class QuaternionFilter
    {
        public static double ki = 0.0;
        public static double kp = 2.0 * 5.0;
        public static double gyroMeasErrornominator = 40.0;
        protected static double gyroMeasError = Math.PI * (gyroMeasErrornominator / 180.0);
    }
}
