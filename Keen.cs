﻿using System;
using System.Diagnostics;
using System.Management.Automation;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class Keen : Updatable
    {
        public enum METHOD { CMD, POWERSHELL }
        private METHOD currentMethod;
        public static METHOD CurrentMethod
        {
            get => instance.currentMethod;
            set => instance.currentMethod = value;
        }

        private static Keen instance;

        public static int controllerID = 1;
        public static string linkToCmdline = @"C:\Joy2OpenVR-0.11b\client_commandline.exe";

        public static double XAxis => 0;
        public static double YAxis => FreedomLocomotion.MoveValue;

        public static int Interval
        {
            set => instance.updateIntervalMs = value;
        }

        private bool started = false;
        private PowerShell powerShell;

        public Keen() : base()
        {
            instance = this;

            runInDispatcher = true;
            updateIntervalMs = 250;
        }

        protected override void Update()
        {
            if (Engine.IsAcceptingClients)
            {
                switch (currentMethod)
                {
                    case METHOD.CMD:
                        ProcessStartInfo psi;
                        Process procCMD;
                        if (!started)
                        {
                            psi = new ProcessStartInfo();
                            psi.WindowStyle = ProcessWindowStyle.Hidden;
                            psi.FileName = "cmd.exe";
                            psi.Arguments = string.Format("/C {1} buttonevent touchandhold {0} 32", controllerID, linkToCmdline);
                            procCMD = new Process();
                            procCMD.StartInfo = psi;
                            procCMD.Start();

                            started = true;
                        }

                        psi = new ProcessStartInfo();
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.FileName = "cmd.exe";
                        psi.Arguments = string.Format("/C {3} axisevent {0} 0 {1} {2}", controllerID, XAxis, YAxis, linkToCmdline);
                        procCMD = new Process();
                        procCMD.StartInfo = psi;
                        procCMD.Start();
                        break;

                    case METHOD.POWERSHELL:
                        if (!started)
                        {
                            powerShell = PowerShell.Create();
                            powerShell.AddScript(string.Format("{1} buttonevent touchandhold {0} 32", controllerID, linkToCmdline));
                            powerShell.Invoke();

                            started = true;
                        }

                        powerShell.Commands.Clear();
                        powerShell.AddScript(string.Format("{3} axisevent {0} 0 {1} {2}", controllerID, XAxis, YAxis, linkToCmdline));
                        powerShell.Invoke();
                        break;
                }
            }
        }
    }
}
//using System;
//using System.Diagnostics;

//namespace treadmill_v2_receive_and_adapt_trackers_data
//{
//    public class Keen : Updatable
//    {
//        private static Keen instance;

//        public static int controllerID = 0;
//        public static string linkToCmdline;

//        public static double XAxis => 0;
//        public static double YAxis => FreedomLocomotion.MoveValue;

//        bool started = false;

//        public Keen() : base()
//        {
//            instance = this;

//            runInDispatcher = false;
//            updateIntervalMs = 250;
//        }

//        protected override void Update()
//        {
//            if (Engine.IsAcceptingClients)
//            {
//                ProcessStartInfo psi;
//                Process procCMD;
//                if (!started)
//                {
//                    psi = new ProcessStartInfo();
//                    psi.WindowStyle = ProcessWindowStyle.Hidden;
//                    psi.FileName = "cmd.exe";
//                    psi.Arguments = string.Format("/C {1} buttonevent touchandhold {0} 32", controllerID, linkToCmdline);
//                    procCMD = new Process();
//                    procCMD.StartInfo = psi;
//                    procCMD.Start();
//                }

//                psi = new ProcessStartInfo();
//                psi.WindowStyle = ProcessWindowStyle.Hidden;
//                psi.FileName = "cmd.exe";
//                psi.Arguments = string.Format("/C {3} axisevent {0} 0 {1} {2}", controllerID, XAxis, YAxis, linkToCmdline);
//                procCMD = new Process();
//                procCMD.StartInfo = psi;
//                procCMD.Start();
//            }

//        }
//    }
//}