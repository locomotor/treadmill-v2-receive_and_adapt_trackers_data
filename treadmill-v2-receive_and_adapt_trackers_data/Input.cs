﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace treadmill_v2_receive_and_adapt_trackers_data
{
    public class Input : Updatable
    {
        private static Input instance;

        private double vertical0;
        private const double vertical0_sensitivity = 3;
        private const double vertical0_gravity = 3;
        private const double vertical0_dead = 0.1;
        private double vertical1;
        private const double vertical1_sensitivity = 3;
        private const double vertical1_gravity = 3;

        public static double Vertical0
        {
            get => Math.Abs(instance.vertical0) < 0.1 ? 0 : instance.vertical0;
            private set => instance.vertical0 = value > 1 ? 1 : (value < -1 ? -1 : value);
        }

        public static double Vertical1
        {
            get => Math.Abs(instance.vertical1) < 0.1 ? 0 : instance.vertical1;
            private set => instance.vertical1 = value > 1 ? 1 : (value < -1 ? -1 : value);
        }

        public static bool A;

        public Input() : base()
        {
            instance = this;
        }

        protected override void Update()
        {
            int shiftMultipier = Keyboard.IsKeyDown(Key.RightShift) || Keyboard.IsKeyDown(Key.LeftShift) ? 5 : 1;

            if (Keyboard.IsKeyDown(Key.P))
                Vertical0 = vertical0 + vertical0_gravity * Time.deltaTime * shiftMultipier;
            else if (Keyboard.IsKeyDown(Key.S))
                Vertical0 = vertical0 - vertical0_sensitivity * Time.deltaTime * shiftMultipier;
            else if (Keyboard.IsKeyUp(Key.W) && Keyboard.IsKeyUp(Key.S))
            {
                int sign = vertical0 > 0 ? 1 : (vertical0 < 0 ? -1 : 0);
                Vertical0 = vertical0 - vertical0_gravity * Time.deltaTime * sign;
            }

            if (Keyboard.IsKeyDown(Key.O))
                Vertical1 = vertical1 + vertical1_gravity * Time.deltaTime * shiftMultipier;
            else if (Keyboard.IsKeyDown(Key.Down))
                Vertical1 = vertical1 - vertical1_sensitivity * Time.deltaTime * shiftMultipier;
            else if (Keyboard.IsKeyUp(Key.Up) && Keyboard.IsKeyUp(Key.Down))
            {
                int sign = vertical1 > 0 ? 1 : (vertical1 < 0 ? -1 : 0);
                Vertical1 = vertical1 - vertical1_gravity * Time.deltaTime * sign;
            }

            A = Keyboard.IsKeyDown(Key.A);
        }
    }
}
